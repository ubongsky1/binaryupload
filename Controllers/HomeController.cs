﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BinaryImage.Models;
using BinaryImage.Data;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace BinaryImage.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult FileUpload()
        {


            return View();
        }

        [HttpPost]
        public async Task<IActionResult> FileUpload(Image image, List<IFormFile> Photos)
        {
            foreach (var item in Photos)
            {
                if (item.Length>0)
                {
                    using (var stream = new MemoryStream())
                    {
                        await item.CopyToAsync(stream);
                        image.Photos = stream.ToArray();
                    }
                }
            }

            _context.Add(image);
            _context.SaveChanges();
            ViewBag.Mess = "file uploaded successfully";

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
