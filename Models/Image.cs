﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BinaryImage.Models
{
    public class Image
    {
        public int Id { get; set; }
        public byte[] Photos { get; set; }
    }
}
